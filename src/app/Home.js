import React from 'react';
import './home.less';
import Header from './Header'
import {BrowserRouter, NavLink} from "react-router-dom";
import CALIMAGE from '../images/calculator.png'
import TIMIMAGE from '../images/timer.png'
import back from '../images/hero-image.png'
const Home = () => {
  return (
      <div className="home">
        <Header/>
        <img src={back} id="backImage"/>
        <div id={'imageHolder'}>
            <img src={CALIMAGE} id={'calImage'}/>
            <img src={TIMIMAGE} id={'timImage'}/>
            <br/>
            <span id={"link"}><NavLink to={'calculator'}>Calculator</NavLink></span>
            <span id={"link"}><NavLink to={'timer'}>Timer</NavLink></span>
        </div>
      </div>)
};

export default Home;