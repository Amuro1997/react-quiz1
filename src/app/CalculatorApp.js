
import React, {Fragment} from 'react'
import MyButton from "./MyButton";
import "./CalculatorApp.less"
import {NavLink} from "react-router-dom";
export default class CalculatorApp extends React.Component{
    constructor(){
        super();
        this.state = {
            label : "",
            op: "",
        }
        this.numberButtonOnClick = this.numberButtonOnClick.bind(this);
        this.equalButtonOnClick = this.equalButtonOnClick.bind(this);
        this.clearButtonOnClick = this.clearButtonOnClick.bind(this);
        this.opButtonOnClick = this.opButtonOnClick.bind(this);
    }

    numberButtonOnClick(event, str){
        this.setState(
            {
                label: this.state.label + str
            }
        )
    }

    equalButtonOnClick(event, str){
        let exp = this.state.label;
        let op = this.state.op;
        let num1 = parseInt(exp.substring(0, exp.indexOf(op)));
        let num2 = parseInt(exp.substring(exp.indexOf(op)+1));
        let res = 0;

        if(op == "+") res = num1 + num2;
        else if(op == "-") res = num1 - num2;
        else res = num1 * num2;

        this.setState({
            label: res.toString()
        });
    }

    clearButtonOnClick(){
        this.setState(
            {label:""}
        );
    }

    opButtonOnClick(event, str){
        this.setState({
            label: this.state.label + str,
            op: str
        })
    }


    render(){
        return (
            <Fragment>
            <div id={'CalculatorAppBody'}>
                <input value={this.state.label} id={'Input'}/>
                <div id={"ButtonGroup"}>
                    <MyButton name="+" onClick={this.opButtonOnClick}/>
                    <MyButton name="-" onClick={this.opButtonOnClick}/>
                    <MyButton name="*" onClick={this.opButtonOnClick}/>
                    <br/>
                    <MyButton name="1" onClick={this.numberButtonOnClick}/>
                    <MyButton name="2" onClick={this.numberButtonOnClick}/>
                    <MyButton name="3" onClick={this.numberButtonOnClick}/>
                    <br/>
                    <MyButton name="4" onClick={this.numberButtonOnClick}/>
                    <MyButton name="5" onClick={this.numberButtonOnClick}/>
                    <MyButton name="6" onClick={this.numberButtonOnClick}/>
                    <br/>
                    <MyButton name="7" onClick={this.numberButtonOnClick}/>
                    <MyButton name="8" onClick={this.numberButtonOnClick}/>
                    <MyButton name="9" onClick={this.numberButtonOnClick}/>
                    <br/>
                    <MyButton name="0" onClick={this.numberButtonOnClick}/>
                    <MyButton name="clear" onClick={this.clearButtonOnClick}/>
                    <MyButton name="=" onClick={this.equalButtonOnClick}/>
                    <br/>
                </div>
            </div>
                <NavLink to={'/'}>Home</NavLink>
            </Fragment>

        );
    }
}