
import React from 'react'
import './MyButton.less'
export default class MyButton extends React.Component{
    constructor(props) {
        super(props);
        this.onClickHelper = this.onClickHelper.bind(this);
    }

    onClickHelper(event){
        this.props.onClick(event, this.props.name);
    }

    render(){
        return (<button onClick={this.onClickHelper}>{this.props.name}</button>)
    }

}