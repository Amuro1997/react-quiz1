
import React from 'react'
import {NavLink} from "react-router-dom";
import './timer.less'
export default class Timer extends React.Component{
    constructor(){
        super();
        this.state = {
            buttonLabel: "start",
            value: 0,
            temp: 0,
            status:'off'
        }
        this.displayValue = this.displayValue.bind(this);
        this.turnTheButtonOn = this.turnTheButtonOn.bind(this);
        this.updateTime =this.updateTime.bind(this);
        setInterval(()=>{
            this.updateTime();
        }, 1000)
    }

    updateTime(){
        if(this.state.status=="on")
            this.setState({
                temp:this.state.temp - 1
            })
        if(this.state.temp <= 0 && this.state.status=='on')
            this.setState({
                status:'off',
                buttonLabel :'start'
            })
    }

    displayValue(event){
        this.setState({
           value : event.currentTarget.value
        });
    }

    turnTheButtonOn(){
        if(this.state.status === 'on')
            return;

        this.setState({
            temp: this.state.value,
            status:'on',
            buttonLabel: 'end'
        })
    }

    render(){
        return (
            <div id={'timerBody'}>
                <h1 id={'title'}>timer</h1>
                <label id={'label'}>set Time: </label>
                <input id={'timerInput'} placeholder={'Please enter time'} width={'1000px'} onChange={this.displayValue}
                value={this.state.value}/>
                {this.state.status == 'off' ? null : <h1>{this.state.temp}</h1>}
                <br/>
                <button id='startButton' onClick={this.turnTheButtonOn}>{this.state.buttonLabel}</button>
                <br/>
                <NavLink to={'/'}>home</NavLink>
            </div>
        )
    }
}