import React, {Fragment} from 'react'
import CalculatorApp from './CalculatorApp'
import {NavLink} from "react-router-dom";
import Header from "./Header";
import './CalculatorBody.less'
export default class extends React.Component{

    constructor(){
        super();
    }

    render(){
        return (
            <Fragment>
                <Header/>
                <div id={'CalculatorBody'}>
                <h1>Calculator</h1>
                <CalculatorApp/>
                </div>
            </Fragment>
        )
    }

}