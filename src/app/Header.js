import React from 'react'
import {Route, BrowserRouter, Link} from "react-router-dom";
import './Header.less'

const Header = ()=>{
    return (
        <header>
            <span id={"nav"}><Link to={'/'}>home</Link></span>
            <span id={"nav"}><Link to={'calculator'}>Calculator</Link></span>
            <span id={"nav"}><Link to={'timer'}>timer</Link></span>

        </header>
    )
}

export default Header;